package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.PsDict;

/**
* service interface
*
* @author liubing
* @date 2016/12/21 17:32
* @version v1.0.0
*/
public interface IPsDictService extends IBaseService<PsDict>,IPaginationService<PsDict>  {
}

